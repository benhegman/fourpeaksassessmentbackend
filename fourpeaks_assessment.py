"""
A script which takes fish tracking data and cleans it up and formats it to the format desired for a Django Rest API
so it can be displayed on an interactive web map.
Date: October 4th 2020
Python: 3.7.9
Author: Ben Hegman

All code developed by Ben Hegman for the sole purpose of the Four Peaks Environmental interview assessment.
"""

import pandas as pd
pd.options.mode.chained_assignment = None
from pyproj import Proj
import json
# loads release and collect data

release = pd.read_excel('four_peaks_assessment/FishData/PIT_CE.xlsx', sheet_name='Release')
collect = pd.read_excel('four_peaks_assessment/FishData/PIT_CE.xlsx', sheet_name='Collection')
release_collect = release.merge(collect, how='outer')
release_collect['time_to_collection'] = (release_collect['Detection Time'] - release_collect['Release Date'])

# loads fish position data
fish_position = pd.read_csv('four_peaks_assessment/FishData/fishPos_20190604.csv')

# adding a new column that is the abbreviation of the AT tag so we can match fish position to the release_collect info
fish_position['at_tag_abv'] = fish_position['Tag_code'].str.slice(start=3, stop=7).str.lower()


# view min max of coordinates
x_min, xmax = fish_position['X'].min(), fish_position['X'].max()
y_min, ymax = fish_position['Y'].min(), fish_position['Y'].max()
# print some stats for the MSE - some data will need to be thrown out as the accuracy of measurement is way off
position_stats = fish_position['MSE'].describe()
# taking entries with MSE below the 75% percentile - about a 5% error
series = fish_position['MSE']
# creates mask - true where mse is below 75th percentile and false elsewhere
rme_exists = fish_position['MSE']<series.quantile(.75)
# reducing the df by the mask
fish_position_reduced = fish_position[rme_exists]

# converting the local coords into degrees/wgs84 which is what mapbox expects
wgs84_proj = Proj("+proj=utm +zone=10, +ellps=WGS84 +datum=WGS84 +units=m +no_defs")
lon, lat = wgs84_proj(fish_position_reduced['X'].values*10+787970, fish_position_reduced['Y'].values*10+5093410, inverse=True)
# add coordinate column
fish_position_reduced['coords'] = list(zip(lon, lat))

# associate each coord with a time and groupby tag abbreivation
fish_position_reduced['Date_time'] = pd.to_datetime(fish_position_reduced['Date_time'], infer_datetime_format=True)
fish_position_reduced['time_coords'] = fish_position_reduced.apply(lambda row: {'date':row['Date_time'],'coords':row['coords']}, axis=1)
coords_dates_grouped = fish_position_reduced.groupby('at_tag_abv')['time_coords'].apply(list)

# function that takes the coordinate dict from each row and returns only the key, value pairs that are closest to each
# hour in the time between release and detection - significantly reduces data that is not needed for bigger picture
# movement patterns
def select_records(element):
    df = pd.DataFrame(element)
    date_range = pd.date_range(start=df.iloc[0]['date'], end=df.iloc[-1]['date'], freq='H')
    df.set_index('date', inplace=True)
    keepers = []
    for date in date_range:
        closest = df.iloc[df.index.get_loc(date,method='nearest')]
        keepers.append({'date':closest.name, 'coords':closest.coords})
    keepers.append({'date':df.iloc[-1].name, 'coords':df.iloc[-1].coords})
    return keepers

final_tracking = coords_dates_grouped.apply(lambda x: select_records(x))


# with open('four_peaks_assessment/FishData/fish.json', 'w') as fp:
#     json.dump(final_tracking, fp, default=str)

# save to pkl for later
final_tracking.to_pickle('four_peaks_assessment/FishData/fish.pkl')

# merge coord data with the data that doesnt have coord info
final_merge = final_tracking.merge(release_collect, left_on='at_tag_abv', right_on='Acoustic Tag', how='outer')
# change date to string for serialization
final_merge['release_date'] = final_merge['Release Date'].dt.strftime('%Y-%m-%d %H:%M:%S')
final_merge['detection_time'] = final_merge['Detection Time'].dt.strftime('%Y-%m-%d %H:%M:%S')

# drop because they were renamed
final_merge.drop(columns=['Release Date', 'Detection Time'], inplace=True)
# rename to more api friendly formats
final_merge.rename(columns={'Species Name':'species_name', 'Site Name':'site_name', 'Antenna Group Name':'antenna', "Tag Code":"tag_code", 'Acoustic Tag':'acoustic_tag'}, inplace=True)

# df to dictionary (json format)
fish_dict = final_tracking.to_dict('index')

# filter each dictionary item so that only unique entries are kept
for key, value in fish_dict.items():
    coords = value['time_coords']
    if isinstance(coords, list):
        res_list = {frozenset(item.items()) : item for item in coords}.values()
        fish_dict[key]['time_coords'] = list(res_list)


# final_tracking_cleaned = pd.DataFrame.from_dict(fish_dict, orient='index')
# final_tracking_cleaned.to_pickle('four_peaks_assessment/FishData/fish_final_dataframe.pkl')

final_tracking = pd.read_pickle('four_peaks_assessment/FishData/fish_final_dataframe.pkl')
final_tracking['site_name'] = final_tracking['site_name'].str.strip()

with open('four_peaks_assessment/FishData/fish_pos_data.json', 'w') as fp:
    json.dump(fish_dict, fp, default=str)






