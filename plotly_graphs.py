"""
A script which creates plotly graphs for fish data provided by Four Peaks Environmental
Date: October 6th 2020
Python: 3.7.9
Author: Ben Hegman

All code developed by Ben Hegman for the sole purpose of the Four Peaks Environmental interview assessment.
"""

import pandas as pd
import plotly.graph_objects as go
import numpy as np
import chart_studio
import chart_studio.plotly as py
from shapely.geometry import LineString

# read in final reduced pkl file
fish_data = pd.read_pickle('four_peaks_assessment/FishData/fish_final_dataframe.pkl')
# create linestrings from the time_coords column to be used to measure distance traveled
time_coords  = fish_data[fish_data['time_coords'].notnull()]
time_coords['distance_in_meters'] = time_coords['time_coords'].apply(lambda x: LineString([y['coords'] for y in x]).length)
time_coords['species_name'].fillna('Unknown', inplace=True)

# work with a dataframe without the coords for the percentage that reached the final collection point.
fish_without_coords = fish_data.drop(columns='time_coords')
fish_without_coords['site_name'] = fish_without_coords['site_name'].str.strip() # strip trailing whitespace on collection points
fish_without_coords['hours_to_detection'] = fish_without_coords["time_to_collection"]/np.timedelta64(1, 'h')
fish_without_coords['species_name'].fillna('Unknown', inplace=True)

species_counts = fish_without_coords['species_name'].value_counts()
number_final_collection = fish_without_coords[fish_without_coords['site_name']=='Final Collection Point']['species_name'].value_counts()
number_final_collection = number_final_collection.append(pd.Series([0], index=['Unknown']))
colors = {'Coho':{'color': 'rgba(93, 164, 214, 0.5)'},'Steelhead':{'color':'rgba(44, 160, 101, 0.5)'},'Chinook':{'color':'rgba(255, 65, 54, 0.5)'},'Cutthroat Trout':{'color':'rgba(255, 144, 14, 0.5)'}, 'Unknown':{'color':'rgba(255, 144, 14, 0.5)'}}

# box and whisker plot for the distance traveled by species
def distance_box():
    fig = go.Figure()
    # add a box/whisker for each species
    for species in time_coords['species_name'].value_counts().index.values:
        print(species)
        df_species = time_coords[time_coords['species_name'] == species]

        fig.add_trace(go.Box(
            y=df_species['distance_in_meters'],
            name=species,
            boxpoints='suspectedoutliers',
            marker_color='rgb(107,174,214)',
            line_color='rgb(107,174,214)',
            fillcolor=colors[species]['color']

        ))
    fig.update_layout(title_text="Distance Traveled by Species", yaxis_title='Distance Traveled (m)', xaxis_title='Species' )
    # save to plotly
    py.plot(fig, filename='boxWhisker', auto_open=True)
distance_box()

# bar chart with counts
def bar_chart():
    x = species_counts.index.values
    y = species_counts.values
    y_pct = np.round(number_final_collection.values/y*100, 2)
    annotations = {
        'Coho':{'x':0, 'text':y_pct[0]}, 'Steelhead':{'x':1,  'text':y_pct[1]}, 'Chinook':{'x':2, 'text':y_pct[2]}, 'Cutthroat Trout':{'x':3,  'text':y_pct[3]}, 'Unknown':{'x':4, 'text':y_pct[4]}
    }
    fig = go.Figure(data=[go.Bar(
        name='Total # Fish Released',
        x=x, y=y,
        text=y,
        textposition='auto',
        marker_color=['rgba(255, 144, 14, 0.5)', 'rgba(255, 144, 14, 0.5)', 'rgba(255, 144, 14, 0.5)', 'rgba(255, 144, 14, 0.5)', 'rgba(255, 144, 14, 0.5)']
    )])
    fig.add_trace(go.Bar(
        x=x,
        y=number_final_collection.values,
        name='# Fish Reached Final Collection Point',
        marker_color=['rgba(44, 160, 101, 0.5)', 'rgba(44, 160, 101, 0.5)', 'rgba(44, 160, 101, 0.5)', 'rgba(44, 160, 101, 0.5)', 'rgba(44, 160, 101, 0.5)'],
        text=number_final_collection.values,
        textposition='auto',
    ))
    # add a note with percentages that made it to final collection point for each species
    for species in species_counts.index.values:
        fig.add_annotation(
            x=annotations[species]['x'],
            y=1500,
            xref="x",
            yref="y",
            text=f"<b>{annotations[species]['text']}%</b> of the {species}s<br> made it to the <br> Final Collection Point",
            font=dict(
                family="Courier New, monospace",
                size=12,
                color="#ffffff"
            ),
            align="center",

            bordercolor="#c7c7c7",
            borderwidth=2,
            borderpad=4,
            bgcolor="#ff7f0e",
            opacity=0.8
        )

    fig.update_layout(
        title='Fish Count by Species',
        xaxis=dict(title='Species', titlefont_size=16,
            tickfont_size=14,),
        xaxis_tickfont_size=14,
        yaxis=dict(
            title='Count',
            titlefont_size=16,
            tickfont_size=14,
        ))

    py.plot(fig, filename='barChart', auto_open=False)
bar_chart()

# filter for fish that were detected
detected_fish = fish_without_coords[fish_without_coords['hours_to_detection']>0]
hours_to_detection = detected_fish['hours_to_detection']
detected_species = detected_fish['species_name'].value_counts()
# box and whisker plots
def box_whisker():
    fig = go.Figure()
    # box and whisker for each species
    for species in detected_species.index.values:
        df_species = detected_fish[detected_fish['species_name']==species]
        df = df_species.loc[df_species['site_name']=='Final Collection Point']
        fig.add_trace(go.Box(
            y=df['hours_to_detection'],
            name=species,
            boxpoints='suspectedoutliers',
            marker_color='rgb(107,174,214)',
            line_color='rgb(107,174,214)',
            fillcolor=colors[species]['color']

        ))
    fig.update_layout(title_text="Fish Detection Statistics by Species", yaxis_title='# of Hours from Release Point to Final Collection Point', xaxis_title='Species' )

    py.plot(fig, filename='boxWhiskerTime', auto_open=False)
box_whisker()

# username = 'benhegman'
# api_key = <api_key>
# chart_studio.tools.set_credentials_file(username=username, api_key=api_key)

