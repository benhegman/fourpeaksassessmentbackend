from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'fish', views.FishViewSet, basename='fish')
router.register(r'fish-position', views.FishPositionViewSet,
                basename='fish-position')


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # path('test/', views.TestView)
]
