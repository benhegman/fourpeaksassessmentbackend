from django.core.management.base import BaseCommand
from fish_api.models import Fish

class Command(BaseCommand):
    def handle(self, *args, **options):
        Fish.objects.all().delete()