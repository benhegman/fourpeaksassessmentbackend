import json
import os
from django.core.management.base import BaseCommand
from fish_api.models import Fish, FishPosition
from django.conf import settings
import datetime


class Command(BaseCommand):
    help = "load fish from a json"

    def handle(self, *args, **options):
        self.create_fish()

    def create_fish(self):
        fishes = self.load_fish('FishData/fish_pos_data.json')
        for fish_id, data in fishes.items():
            if isinstance(data['time_coords'], list):
                time_coords = data.pop('time_coords')
                new_fish, created = Fish.objects.get_or_create(id=fish_id, defaults=data)
                if created:
                    self.stdout.write(self.style.SUCCESS(f'created fish with id {new_fish.id}'))
                    for position in time_coords:
                        FishPosition.objects.get_or_create(date_time = datetime.datetime.strptime(position['date'], '%Y-%m-%d %H:%M:%S.%f'), coordinates = position['coords'], fish=new_fish)

                else:
                    self.stdout.write(self.style.WARNING(f'fish with id {new_fish.id} already exists'))



    def load_fish(self, json_file_path):
        print(settings.BASE_DIR)
        with open(os.path.join(settings.BASE_DIR, json_file_path)) as json_file:
            return json.load(json_file)

