from django.http import JsonResponse
from rest_framework import viewsets
from rest_framework import permissions
from .serializers import FishSerializer, FishPositionSerializer
from .models import Fish, FishPosition
import json

# Create your views here.


# def TestView(request, *args, **kwargs):
#     data = {
#         "id": [['hi', 'hello'], ['ye', 'yeah']],
#         "content": "A test!"
#     }
#     return JsonResponse(data)


class FishViewSet(viewsets.ModelViewSet):
    serializer_class = FishSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        fish = Fish.objects.all()
        species_name = self.request.query_params.get('species_name', None)
        site_name = self.request.query_params.get('site_name', None)

        if species_name is not None:
            try:
                fish = fish.filter(species_name__in=species_name.split(','))
            except:
                return fish

        if site_name is not None:
            try:
                fish = fish.filter(site_name__in=site_name.split(','))
            except:
                return fish
        return fish


class FishPositionViewSet(viewsets.ModelViewSet):
    serializer_class = FishPositionSerializer
    permission_classes = [permissions.IsAuthenticated]
    queryset = FishPosition.objects.all()
