from django.apps import AppConfig


class FishApiConfig(AppConfig):
    name = 'fish_api'
