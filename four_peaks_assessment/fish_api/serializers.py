from rest_framework import serializers
from .models import Fish, FishPosition


class FishPositionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FishPosition
        fields = ('date_time', 'coordinates', 'fish')


class FishSerializer(serializers.HyperlinkedModelSerializer):
    fish_coordinates = FishPositionSerializer(many=True)

    class Meta:
        model = Fish
        fields = ('id', 'acoustic_tag', 'species_name', 'site_name', 'antenna', 'time_to_collection', 'release_date', 'detection_time', 'tag_code', 'fish_coordinates')