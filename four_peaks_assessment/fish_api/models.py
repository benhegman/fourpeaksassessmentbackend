from django.db import models
from django.contrib.postgres.fields import ArrayField
# from django.contrib.gis.db import models as geomodels



# Create your models here.
class Fish(models.Model):
    id = models.AutoField(primary_key=True)
    acoustic_tag = models.CharField(max_length=60, blank=True)
    tag_code = models.CharField(max_length=60, blank=True)
    species_name = models.CharField(max_length=60, blank=True)
    site_name = models.CharField(max_length=60, blank=True)
    antenna = models.CharField(max_length=60, blank=True)
    time_to_collection = models.CharField(max_length=60, blank=True)
    release_date = models.CharField(max_length=60, blank=True)
    detection_time = models.CharField(max_length=60, blank=True)
    def __str__(self):
        return self.acoustic_tag

class FishPosition(models.Model):
    fish = models.ForeignKey(Fish, on_delete=models.CASCADE, related_name='fish_coordinates')
    date_time = models.DateTimeField()
    coordinates = ArrayField(models.FloatField(null=True, blank=True))


