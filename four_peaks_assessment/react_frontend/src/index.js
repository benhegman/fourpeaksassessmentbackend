import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./components/App";

/*
In normal javascript you use assign a variable by doing: 
var app = document.getElementById("app")
// and then do what you want with that DOM Node - we could attach an event listener, delete it etc...

In React its essentially the same just different syntax - below it's attaching the component App to
the div element "app"

This index.js file is treated as an entry point for webpack which looks in here, bundles any JS and 
outputs it to main.js which is what gets loaded by django/browser.
 */
// if ( document.URL.includes("homepage.aspx") ) {
//     //Code here
// }

if (document.URL === "http://127.0.0.1:8000/react/") {
  ReactDOM.render(<App />, document.getElementById("app"));
} else if (document.URL === "http://127.0.0.1:8000/react/two/") {
  ReactDOM.render(<App />, document.getElementById("test"));
}
