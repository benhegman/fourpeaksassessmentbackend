import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  ListItemIcon,
  ListItemText,
  Collapse,
  ListItem,
  List,
  ListItemSecondaryAction,
  Checkbox,
} from "@material-ui/core";
import Waves from "@material-ui/icons/Waves";
import Place from "@material-ui/icons/Place";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import axios from "axios";
import useStore from "../../../store";
// styles from material-ui
const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 300,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

const databaseUrl = process.env.REACT_APP_DATABASE;
// match the mapping of filter options with their labels and filter strings to be added to api url
const speciesToggle = { 0: "Coho", 1: "Chinook", 2: "Steelhead", 3: "nan" };
const collectToggle = {
  0: "Final Collection Point",
  1: "Collection Point 3",
  2: "Collection Point 4",
  3: "nan",
};

export default function NestedList(props) {
  const mapData = useStore((state) => state.mapData);
  const setMapData = useStore((state) => state.setMapData);
  const classes = useStyles();

  // states for both the species and collection point filters
  const [open, setOpen] = useState(false);
  const [checked, setChecked] = useState([0, 1, 2, 3]);
  const [collectCheck, setCollectCheck] = useState([0, 1, 2, 3]);
  const [collectOpen, setCollectOpen] = useState(false);

  // click handler for the species filters
  const handleClick = () => {
    setOpen(!open);
  };
  // click handler for the collection point filter options - makes the checkboxes work
  const handleCollectToggle = (value) => () => {
    const currentIndex = collectCheck.indexOf(value);
    const newChecked = [...collectCheck];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setCollectCheck(newChecked);
  };

  // toggle hanler for the species filter options
  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  useEffect(() => {
    const speciesFilterUrl = checked
      .map((option) => {
        return speciesToggle[option];
      })
      .toString();
    const siteFilterUrl = collectCheck
      .map((option) => {
        return collectToggle[option].replace(/ /g, "+");
      })
      .toString();
    async function fetchData() {
      let res = await axios.get(
        `${databaseUrl}?species_name=${speciesFilterUrl}&site_name=${siteFilterUrl}`
      );
      let data = {
        type: "FeatureCollection",
        features: res.data.map((fish) => {
          return {
            type: "Feature",
            properties: { ...fish },
            geometry: {
              type: "Point",
              coordinates: JSON.parse(fish.time_coords.replace(/'/g, '"'))[0] // create a array of dicts (coords) from string
                .coords,
            },
          };
        }),
      };
      props.map.getSource("fish").setData(data);
      setMapData(data);
    }
    fetchData(); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [checked, collectCheck]);

  return (
    <List
      dense={true}
      component="nav"
      aria-labelledby="nested-list-subheader"
      className={classes.root}
    >
      <ListItem
        button
        onClick={() => {
          setCollectOpen(!collectOpen);
        }}
      >
        <ListItemIcon>
          <Place />
        </ListItemIcon>
        <ListItemText primary="Collection Point" />
        {collectOpen ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      {/* the collapse options and checkboxes for collection point */}
      <Collapse in={collectOpen} timeout="auto" unmountOnExit>
        <List dense={true} className={classes.root}>
          {[0, 1, 2, 3].map((value) => {
            const labelId = `checkbox-list-secondary-label-${value}`;
            return (
              <ListItem key={collectToggle[value]} button>
                <ListItemText id={labelId} primary={collectToggle[value]} />
                <ListItemSecondaryAction>
                  <Checkbox
                    edge="end"
                    onChange={handleCollectToggle(value)}
                    checked={collectCheck.indexOf(value) !== -1}
                    inputProps={{ "aria-labelledby": labelId }}
                  />
                </ListItemSecondaryAction>
              </ListItem>
            );
          })}
        </List>
      </Collapse>
      <ListItem button onClick={handleClick}>
        <ListItemIcon>
          <Waves />
        </ListItemIcon>
        <ListItemText primary="Species" />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      {/* the collapsed option for the Species filters */}
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List dense={true} className={classes.root}>
          {[0, 1, 2, 3].map((value) => {
            const labelId = `checkbox-list-secondary-label-${value}`;
            return (
              <ListItem key={speciesToggle[value]} button>
                <ListItemText id={labelId} primary={speciesToggle[value]} />
                <ListItemSecondaryAction>
                  <Checkbox
                    edge="end"
                    onChange={handleToggle(value)}
                    checked={checked.indexOf(value) !== -1}
                    inputProps={{ "aria-labelledby": labelId }}
                  />
                </ListItemSecondaryAction>
              </ListItem>
            );
          })}
        </List>
      </Collapse>
    </List>
  );
}
