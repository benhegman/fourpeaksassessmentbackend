import React, { useState, useRef, useEffect } from "react";
import "./timeline.css";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { Slider, Snackbar, IconButton } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import * as datefns from "date-fns";
import { Modal, Popover } from "react-bootstrap";
import NestedList from "./FilterList";
import * as turf from "@turf/turf";
import useStore from "../../../store";

// when you initialize a function (component) you pass the properties it receives from its parent component as a parameter (usually named props but you can call it whatever)
// when you want to access those props just use dot notation: let fishDates = props.fishDates
export default function Timeline(props) {
  const [isPlaying, setIsPlaying] = useState(false); // controls fish playing
  const [sliderMarks, setSliderMarks] = useState(null);
  const [hoursDiff, setHoursDiff] = useState(null); // hours between start and end time
  const isFirstRun = useRef(true); // so that use effects dont run on the first go
  const [showGraphs, setShowGraphs] = useState(false);

  // state is basically a variable that is tracked by react so when we use useState() on the initialization of a variable it becomes state.
  // any size component can have its own state.  Any time a state is changed, React will look to see which components use that state as a
  // property (usually called props) and then rerender just that component.  For example we use this slider value in the timeline so every
  // time the slidervalue is updated the timeline slider is rerender with the updated value
  // [theState, a function to set that specific state]
  const [sliderValue, setSliderValue] = useState(0);
  const [traveled, setTraveled] = useState(0); //state for distance traveled
  const [step, setStep] = useState(0);
  const [open, setOpen] = useState(false);
  //const mapData = useStore(state => state.mapData).length
  const valuetext = (value) => {
    return value;
  };

  // toggle for the play button in the toolbar - lineShowing state must be set before playing
  function toggle() {
    if (props.lineShowing) {
      setIsPlaying(!isPlaying);
    } else alert("Please Select A Fish First");
  }

  // resets the timeline and fish along the path.  Called anytime the fish selected/lineshowing state is changed
  function reset() {
    setStep(0);
    setIsPlaying(false);
    // turf finds point along a line at specified distance and then it updates the point at that point along the line
    props.map.getSource("fishPoint").setData(turf.along(props.lineShowing, 0));
    setSliderValue(0);
    setOpen(false);
  }
  // use effect which opens the snackbar notification everytime the play button is pressed
  useEffect(() => {
    if (isPlaying) {
      setOpen(true);
    }
  }, [isPlaying]);

  // close logic for the snackbar - only close by clicking the x
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  // takes the line and calculates the distance - used in snackbar and for updating the fishes location along the path
  const calculatePathDistance = (path) => {
    return turf.length(path, { units: "meters" });
  };

  // called everytime the slider value changes
  const updateFishLocation = () => {
    const line = props.lineShowing;
    const lineDistance = calculatePathDistance(line);
    setTraveled(lineDistance);
    const fishChangePerStep = lineDistance / 500;
    let pointOnLine = turf.along(line, fishChangePerStep * step * 2, {
      units: "meters",
    });
    props.map.getSource("fishPoint").setData(pointOnLine);
  };

  // when lineShowing state changes, reset the timeline slider values
  useEffect(() => {
    if (isFirstRun.current) {
      isFirstRun.current = false;
      return;
    } else {
      reset();
    } // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.lineShowing]);

  // allows the timeline and fish location to update - steps are set to 500.  More is smoother but more computation
  useEffect(() => {
    let interval = null;
    const timeLineChangePerStep = hoursDiff / 11.7234468938 / 500;
    // updates fish location every step if isPlaying is true and steps are below the max steps
    if (isPlaying && step < 251) {
      interval = setInterval(() => {
        setStep((step) => step + 1);
        setSliderValue((sliderValue) => sliderValue + timeLineChangePerStep);
        updateFishLocation();
      }, 60);
    } else if (!isPlaying && step !== 0) {
      clearInterval(interval);
    } else if (step === 251) {
      setIsPlaying();
    }

    return () => clearInterval(interval); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isPlaying, step]);

  // calculates the marks and labels for the timeline for each fish clicked
  // done by finding the start and end date and finding the time in between them and making labels accordingly
  const marks = () => {
    if (props.fishDetails) {
      const fishDates =
        props.fishDetails.detection_time !== "nan"
          ? [props.fishDetails.release_date, props.fishDetails.detection_time]
          : [props.fishDetails.release_date, props.fishDates[1]];
      const start =
        props.fishDetails.release_date !== "nan"
          ? datefns.parse(fishDates[0], "yyyy-MM-dd HH:mm:ss", new Date())
          : datefns.parse(
              props.fishDates[0],
              "yyyy-MM-dd HH:mm:ss.SSSSSS",
              new Date()
            );

      const end =
        props.fishDetails.detection_time !== "nan"
          ? datefns.parse(fishDates[1], "yyyy-MM-dd HH:mm:ss", new Date())
          : datefns.parse(
              fishDates[1],
              "yyyy-MM-dd HH:mm:ss.SSSSSS",
              new Date()
            );
      const daysDiff = datefns.differenceInDays(end, start);
      const hoursDiff = datefns.differenceInHours(end, start);
      setHoursDiff(hoursDiff);
      const labelArray = [...Array(daysDiff + 1).keys()].filter((element) => {
        return element % 2 === 0;
      });

      const labels = labelArray.map((day) => {
        return {
          value: day,
          label: datefns.format(datefns.addDays(start, day), "MMM do"),
        };
      });
      setSliderMarks(labels);
    } else {
    }
  };

  // when dates are updated update the marks on the timeline
  useEffect(() => {
    if (isFirstRun.current) {
      isFirstRun.current = false;
      return;
    } else {
      marks();
    } // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.fishDates]);

  return (
    <div>
      <div className="row mb-3 timeline">
        <div className="col-3 text-right">
          <OverlayTrigger
            key="filter"
            trigger="click"
            placement="top"
            overlay={
              <Popover id={"filter-content"}>
                <Popover.Title>
                  Filter by Species or Collection Point
                </Popover.Title>
                <Popover.Content>
                  <NestedList map={props.map}></NestedList>
                </Popover.Content>
              </Popover>
            }
          >
            <button className="btn filter">
              <i className="material-icons md-36">filter_list</i>
              <h6 className="text-center">Filter</h6>
            </button>
          </OverlayTrigger>
          <OverlayTrigger
            key={"tooltip"}
            placement={"top"}
            overlay={
              <Tooltip>{showGraphs ? "Hide" : "View"} Fish Statistics</Tooltip>
            }
          >
            <button
              className="btn"
              onClick={() => {
                setShowGraphs(!showGraphs);
              }}
            >
              <i className="material-icons md-36">analytics</i>
            </button>
          </OverlayTrigger>
          <Modal
            show={showGraphs}
            onHide={() => setShowGraphs(false)}
            dialogClassName="custom-dialog"
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="example-custom-modal-styling-title">
                Fish Statistics
              </Modal.Title>
            </Modal.Header>
            <Modal.Body className="custom-dialog">
              <div className="plots slick-slider">
                {" "}
                {/* embeds the plots created with plotly in using iframe */}
                <iframe
                  title="bar_plot"
                  width="90%"
                  height="600"
                  frameBorder="0"
                  scrolling="no"
                  src="//plotly.com/~benhegman/3.embed"
                ></iframe>
                <iframe
                  title="box_whisker"
                  width="90%"
                  height="600"
                  frameborder="0"
                  scrolling="no"
                  src="//plotly.com/~benhegman/6.embed"
                ></iframe>
                <iframe
                  title="box_whisker_distance"
                  width="90%"
                  height="600"
                  frameborder="0"
                  scrolling="no"
                  src="//plotly.com/~benhegman/1.embed"
                ></iframe>
              </div>
            </Modal.Body>
          </Modal>

          <OverlayTrigger
            key={"animate"}
            placement={"top"}
            overlay={<Tooltip>Animate Fish Activity</Tooltip>}
          >
            <button onClick={toggle} className="btn">
              <i className="material-icons md-36">
                {isPlaying ? "pause_circle_outline" : "play_circle_outline"}
              </i>
            </button>
          </OverlayTrigger>
        </div>
        <div className="col-8">
          {" "}
          {/* This is a component from an external source (imported at the top) and it can take several props.  I passed the sliderValue state as a prop
          so that when its updated the slider also is updated 
          https://material-ui.com/api/slider/ */}
          <Slider
            value={sliderValue}
            className="my-auto"
            defaultValue={0}
            getAriaValueText={valuetext}
            aria-labelledby="discrete-slider-small-steps"
            step={0.5}
            marks={sliderMarks}
            min={0}
            max={sliderMarks ? Object.keys(sliderMarks).length * 2 - 1 : null}
            valueLabelDisplay="auto"
            onChange={(e, value) => {
              setSliderValue(value);
            }}
          />
        </div>
        <div className="col-1 notis">
          <OverlayTrigger
            key={"reset"}
            placement={"top"}
            overlay={<Tooltip>Reset Fish Path</Tooltip>}
          >
            <button className="btn text-center">
              <i className="material-icons md-36" onClick={reset}>
                loop
              </i>
            </button>
          </OverlayTrigger>
        </div>
      </div>
      <div>
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "left" }}
          open={open}
          onClose={handleClose}
          message={
            open
              ? `This ${
                  props.fishDetails.species_name
                } traveled ${traveled.toFixed(1)} meters over the course of ${(
                  hoursDiff / 24
                ).toFixed(1)} days!`
              : ""
          }
          key={"fishSnack"}
          action={
            <React.Fragment>
              <IconButton
                size="small"
                aria-label="close"
                color="inherit"
                onClick={handleClose}
              >
                <CloseIcon fontSize="small" />
              </IconButton>
            </React.Fragment>
          }
        />
      </div>
    </div>
  );
}
