"""
views
"""
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from fish_api.models import Fish
# Create your views here.


@login_required(login_url='/accounts/login')
def index(request):
    """view for the main react page"""

    fish = Fish.objects.first()
    user = request.user
    return render(request, 'react_frontend/index.html',
                  context={'fish': fish, 'username': user.username})


@login_required(login_url='/accounts/login')
def test(request):
    """view for the main react page"""

    fish = Fish.objects.first()
    user = request.user
    return render(request, 'react_frontend/test.html',
                  context={'fish': fish, 'username': user.username})
